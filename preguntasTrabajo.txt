


HTML
__________________________________________________________________________________________________________________________
1�
(Modelo de objetos)
�Cuantos tipos de Metadatos hay?

5 Estos elementos se encuentran en la cabecera del documento 
7 <base>, <link>, <meta>, <noscript>, <script>, <style>, <title>.
6 <script>, <style>, <title>

2�
(Etiquetas Html)
�Cu�l es el elemento HTML correcto para insertar un salto de l�nea?

<lb>
<br>.
<breack>

3�
(Formularios)
�Cu�l es el HTML correcto para hacer una casilla de verificaci�n?

<check>
<input type="check">
<input type="checkbox">.
<verificaci�n>
4�
(Api de almacenamiento web)
Antes de HTML5, los datos de la aplicaci�n se almacenaban en:

memoria RAM
disco duro
cookies.
memoria interna

5�
(Canvas)
El elemento <canvas> en HTML5 se utiliza para:

dibujar graficos.
crear elemetos arrastables
manipular datos de usuario
recoger token de validaci�n







CSS
_________________________________________________________________________________________________________________________
Pregunta 1:
�Qu� propiedad de CSS se emplea para definir el espacio entre palabras?

 a) word-spacing 

 b) letter-spacing 

 c) white-space 

 d) text-indent 

Pregunta 2:
�Qu� propiedad de CSS se emplea para indicar que un texto se debe mostrar en cursiva (it�lica)?

 a) font-italic 

 b) font-style 

 c) font-variant 

 d) Las anteriores respuestas no son correctas 

Pregunta 3:
�Cu�l es el c�digo XHTML correcto para hacer referencia a una hoja de estilo externa?

 a) <link rel="stylesheet" type="text/css" href="style.css" /> 

 b) <link rel="stylesheet" type="text/css" href="style.css"> 

 c) <link rel=stylesheet type=text/css href=style.css /> 

 d) <link rel=stylesheet type=text/css href=style.css> 

Pregunta 4:
�Cu�l es la sintaxis correcta en CSS?

 a) {body=color:black} 

 b) {body:color=black} 

 c) body {color:black} 

 d) body {color=black} 

Pregunta 5:
�Qu� propiedad de CSS se emplea para cambiar el color de fondo de un elemento?

 a) color 

 b) bgcolor 

 c) backcolor 

 d) background-color 







JavaScript
_________________________________________________________________________________________________________________________
Pregunta 1:
JavaScript fue dise�ado por

 a) Bill Gates 

 b) Bjarne Stroustrup 

 c) Brendan Eich 

 d) Dennis M. Ritchie 

Pregunta 2:
En JavaScript, �c�mo se llama el objeto que representa una expresi�n regular?

 a) No hay ning�n objeto 

 b) ExpReg 

 c) RegExp 

 d) Rexp 

Pregunta 3:
Respecto a los navegadores web, el BOM es el

 a) Byte Object Model 

 b) Browser Object Model 

 c) Binary Object Model 

 d) Las anteriores respuestas no son correctas 

Pregunta 4:
La estandarizaci�n de JavaScript es realizada por

 a) ECMA 

 b) Mozilla 

 c) The Script Consortium 

 d) W3C 

Pregunta 5:
�Qu� funci�n existe en el DOM?

 a) getElementsById() 

 b) getElementByName() 

 c) getElementsByTagName() 

 d) Las anteriores respuestas no son correctas 





PHP
_________________________________________________________________________________________________________________________

Pregunta 1:
En PHP, �qu� muestra el siguiente c�digo?
<?php
$perro= "Gato";
$gato = &$perro;
$gato = "Perro";

echo $perro . " " . $gato;
?>

 a) Perro Gato 

 b) Gato Perro 

 c) Gato Gato 

 d) Perro Perro 

Pregunta 2:
�C�mo se puede asegurar que una p�gina web ha sido desarrollada con PHP?

 a) Viendo el c�digo fuente de la p�gina en el navegador 

 b) Viendo si la extensi�n de la p�gina acaba en .php 

 c) Viendo las cabeceras HTTP de la p�gina 

 d) Nunca se puede estar seguro al 100% de si una p�gina ha sido desarrollada con PHP 

Pregunta 3:
En PHP, �c�mo se crea una cookie?

 a) Con document.cookie 

 b) Con $_COOKIE 

 c) Con setcookie() 

 d) Con doCookie() 

Pregunta 4:
En una aplicaci�n web es importante validar en el lado del servidor porque:

 a) Mejora el tiempo de respuesta de la aplicaci�n 

 b) Reduce la carga de trabajo en el servidor 

 c) Simplifica el c�digo que se debe programar en el servidor 

 d) Las anteriores respuestas no son correctas 

Pregunta 5:
En PHP, �qu� funciones se emplean para abrir y cerrar una conexi�n a una base de datos ODBC?

 a) odbc_open() y odbc_close() 

 b) odbc_open() y odbc_disconnect() 

 c) odbc_connect() y odbc_close() 

 d) odbc_connect() y odbc_disconnect() 







